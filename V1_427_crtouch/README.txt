Ender 3 V1 4.2.7 board
CR touch
Bed Tramming wizard
Z Offset wizard
Fast Probing
UBL Bed Leveling
Hotend Timeout
Z probe offsets
Input shaping 

Changes:

Configuration.h (changes from Marlin Configurationes default Ender 3 427 config):

#2187,2188c
  #define EEPROM_AUTO_INIT  // Init EEPROM automatically on any errors.
  #define EEPROM_INIT_NOW   // Init EEPROM on first boot after a new build.
.
2096c
#define Z_SAFE_HOMING
.
2032c
#define LCD_BED_TRAMMING
.
1995c
  #define UBL_HILBERT_CURVE       // Use Hilbert distribution for less travel when probing multiple points
.
1991c
  #define MESH_INSET 15              // Set Mesh bounds as an inset region of the bed
.
1969c
    #define EXTRAPOLATE_BEYOND_GRID
.
1959c
  #define GRID_MAX_POINTS_X 10
.
1943c
  #define G26_MESH_VALIDATION
.
1894c
#define PREHEAT_BEFORE_LEVELING
.
1888c
#define RESTORE_LEVELING_AFTER_G28
.
1880c
#define AUTO_BED_LEVELING_UBL
.
1736c
  #define MIN_SOFTWARE_ENDSTOP_Y 
.
1707,1709c
#define X_MAX_POS 250
#define Y_MAX_POS 235
#define Z_MAX_POS 235
.
1499c
#define Z_PROBE_FEEDRATE_FAST (20*60)
.
1493c
#define PROBING_MARGIN 5
.
1489c
#define NOZZLE_TO_PROBE_OFFSET { -45, -10, 0 }
.
1337c
#define BLTOUCH
.
1283c
#define USE_PROBE_FOR_Z_HOMING
.
1280c
//#define Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN
.

Configuration_adv.h:

2084c
  #define BABYSTEP_DISPLAY_TOTAL          // Display total babysteps since last G28
.
1378c
  #define PROBE_OFFSET_WIZARD       // Add a Probe Z Offset calibration option to the LCD menu
.
1084,1085c
#define INPUT_SHAPING_X
#define INPUT_SHAPING_Y
.
1047c
  #define ASSISTED_TRAMMING_WIZARD    // Add a Tramming Wizard to the LCD menu
.
1045c
  #define REPORT_TRAMMING_MM          // Report Z deviation (mm) for each point relative to the first
.
1036c
  #define TRAMMING_POINT_XY { {  32, 32 }, { 202,  32 }, { 202, 202 }, { 32, 202 } }
.
1032c
#define ASSISTED_TRAMMING
.
957c
  #define BLTOUCH_HS_MODE true
.
516c
#define HOTEND_IDLE_TIMEOUT
.
504c
#define EXTRUDER_RUNOUT_PREVENT


Ender 3 V2 4.2.2 board
CR touch
Filament Runout Sensor
Bed Tramming wizard
Z Offset wizard
Fast Probing
Z probe offsets { -55, -6, 0 }
Input shaping (expirimental)

Changes:

Configuration.h (changes from Marlin Configurationes default Ender 3 427 config):

#define X_BED_SIZE 235
#define Y_BED_SIZE 235
#define Z_MAX_POS 235

#define EEPROM_SETTINGS  

//#define Z_MIN_PROBE_USES_Z_MIN_ENDSTOP_PIN

// Force the use of the probe for Z-axis homing
#define USE_PROBE_FOR_Z_HOMING

#define BLTOUCH

#define AUTO_BED_LEVELING_BILINEAR

#define PREHEAT_BEFORE_LEVELING
#if ENABLED(PREHEAT_BEFORE_LEVELING)
  #define LEVELING_NOZZLE_TEMP 120   // (°C) Only applies to E0 at this time
  #define LEVELING_BED_TEMP     50
#endif

#define LCD_BED_LEVELING

#define LCD_BED_TRAMMING

#define Z_SAFE_HOMING

#define NOZZLE_PARK_FEATURE

#define NOZZLE_TO_PROBE_OFFSET { -55, -6, 0 }

#define PROBING_MARGIN 10

// X and Y axis travel speed (mm/min) between probes
#define XY_PROBE_FEEDRATE (133*60)

// Feedrate (mm/min) for the first approach when double-probing (MULTIPLE_PROBING == 2)
#define Z_PROBE_FEEDRATE_FAST (20*60)

// Feedrate (mm/min) for the "accurate" probe of each point
#define Z_PROBE_FEEDRATE_SLOW (Z_PROBE_FEEDRATE_FAST / 2)

#define FILAMENT_RUNOUT_SENSOR


Configuration_adv.h:

#define BLTOUCH_HS_MODE true

#define ASSISTED_TRAMMING
#if ENABLED(ASSISTED_TRAMMING)

  // Define positions for probe points.
  #define TRAMMING_POINT_XY { {  20, 20 }, { 180,  20 }, { 180, 202.5 }, { 20, 202.5 } }

  #define ASSISTED_TRAMMING_WIZARD    // Add a Tramming Wizard to the LCD menu

#define INPUT_SHAPING_X
#define INPUT_SHAPING_Y

#if HAS_BED_PROBE && EITHER(HAS_MARLINUI_MENU, HAS_TFT_LVGL_UI)
  #define PROBE_OFFSET_WIZARD       // Add a Probe Z Offset calibration option to the LCD menu

#define ADVANCED_PAUSE_FEATURE